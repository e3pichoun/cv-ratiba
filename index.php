<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="CV de notre amie" />
        <meta name="author" content="e3pichoun/ Julia Carretero" />
        <title>Ratiba Benzerouali</title>
        <link rel="shortcut icon" type="image/png" href="assets/img/favicon.png" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,800,800i" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <?php
        if (empty($_POST)) :
    ?>

    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
            <a class="navbar-brand js-scroll-trigger" href="#page-top">
                <span class="d-block d-lg-none">Ratiba Benzerouali</span>
                <span class="d-none d-lg-block"><img class="img-fluid img-profile rounded-circle mx-auto mb-2" src="assets/img/profile.jpg" alt="" /></span>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#about">À propos</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#experience">Expériences</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#formation">Formation</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#skills">Compétences</a></li>
                   
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#contact">Contact</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="CV .pdf"download>Télécharger mon CV</a></li>
                </ul>
            </div>
        </nav>
        <!-- Contenu de la page-->
        <div class="container-fluid p-0">
            <!-- About-->
            <section class="resume-section" id="about">
                <div class="resume-section-content">
                    <h1 class="mb-0">
                        Ratiba
                        <span class="text-primary">Benzerouali</span>
                    </h1>
                    <div class="subheading mb-5">
                        61 rue Roger Salengo · 93430 Villetaneuse · 07 71 83 30 96 ·
                        tiba.benzer@gmail.com</a>
                    </div>
                    <p class="lead mb-5"><p>Développeuse Web</p><p>Dynamique, autonome et rigoureuse, je suis du passionnée par le numérique, la musique et les voyages.</p>
                    <div class="social-icons">
                        <a class="social-icon" href="https://www.linkedin.com/in/ratiba-benzerouali-5695a8194/"><i class="fab fa-linkedin-in"></i></a>
                        <a class="social-icon" href="https://gitlab.com/tiba.benzer"><i class="fab fa-gitlab"></i></a>
                        <a class="social-icon" href="#"><i class="fab fa-facebook-f"></i></a>
                        <a class="social-icon" href="#"><i class="fas fa-briefcase"></i></a>
                    </div>
                </div>
            </section>
            <hr class="m-0" />
            <!-- Experience Professionelle-->
            <section class="resume-section" id="experience">
                <div class="resume-section-content">
                    <h2 class="mb-5">Expériences</h2>
                    <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                        <div class="flex-grow-1">
                            <h3 class="mb-0">Assistante Marketing</h3>
                            <div class="subheading mb-3">L'Art de Vivre - secteur cosmétiques Shanghai,Chine</div>
                            <ul class="fa-ul mb-0">
                                <li>
                                    <span class="fa-li"><i class="fas fa-check"></i></span>
                                    Community management (planifier,gérer et créer du contenu pour les réseaux sociaux)
                                </li>
                                <li>
                                    <span class="fa-li"><i class="fas fa-check"></i></span>
                                    Créer du contenu marketing pour le site Web
                                </li>
                                <li>
                                    <span class="fa-li"><i class="fas fa-check"></i></span>
                                    Études de marché et veille concurrentielle
                                </li>
                            </ul>
                        </div>
                        <div class="flex-shrink-0"><span class="text-primary">Juillet 2019 - Janvier 2019</span></div>
                    </div>
                    <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                        <div class="flex-grow-1">
                            <h3 class="mb-0">Assistante Achats</h3>
                            <div class="subheading mb-3">Phocéenne de négoce - secteur alimentaire Bouc-Bel-Air, Bouches du Rhône</div>
                            <ul class="fa-ul mb-0">
                                <li>
                                    <span class="fa-li"><i class="fas fa-check"></i></span>
                                    Approvisionner les stocks
                                </li>
                                <li>
                                    <span class="fa-li"><i class="fas fa-check"></i></span>
                                    Comparer les fournisseurs et négocier les prix d'achats
                                </li>
                                <li>
                                    <span class="fa-li"><i class="fas fa-check"></i></span>
                                    Suivre les commandes auprès des fournisseurs
                                </li>
                                <li>
                                    <span class="fa-li"><i class="fas fa-check"></i></span>
                                    Piloter la logistique des produits (Choisir et valider le mode de transport, suivre les opérations de dédouanement et acheminer jusqu'aux entrepôts)
                                </li>
                                <li>
                                    <span class="fa-li"><i class="fas fa-check"></i></span>
                                    Participer à l'élaboration des tarifs de vente
                                </li>
                            </ul>
                            </br>
                            Partie communication
                            <ul class="fa-ul mb-0">
                                <li>
                                    <span class="fa-li"><i class="fas fa-check"></i></span>
                                    Prendre les commandes
                                </li>
                                <li>
                                    <span class="fa-li"><i class="fas fa-check"></i></span>
                                    Facturer et effectuer un suivi clientèle
                                </li>
                                <li>
                                    <span class="fa-li"><i class="fas fa-check"></i></span>
                                    Créer de nouveaux catalogues
                                </li>
                            </ul>
                        </div>
                        <div class="flex-shrink-0"><span class="text-primary">Avril 2018 - Décembre 2018</span></div>
                    </div>
                    <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                        <div class="flex-grow-1">
                            <h3 class="mb-0">Emplois étudiants</h3>
                            <div class="subheading mb-3">Lyon / Romans-sur-Isère/ Marseille</div>
                            <p>Agent de production / Enquêtrise téléphonique TNS Soffres / Hôtesse de caisse Carrefour</p>
                        </div>
                        <div class="flex-shrink-0"><span class="text-primary">2015 - 2017</span></div>
                    </div>
                    <div class="d-flex flex-column flex-md-row justify-content-between">
                        <div class="flex-grow-1">
                            <h3 class="mb-0">Programme Vacances Travail</h3>
                            <div class="subheading mb-3">Australie</div>
                            <p>Restauration, vente, aide ménagère</p>
                        </div>
                        <div class="flex-shrink-0"><span class="text-primary">2013 - 2014</span></div>
                    </div>
                </div>
            </section>
            <hr class="m-0" />
            <!-- Formation-->
            <section class="resume-section" id="formation">
                <div class="resume-section-content">
                    <h2 class="mb-5">Formation</h2>
                    <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                    <div class="flex-grow-1">
                            <h3 class="mb-0">Développeur Web et Web Mobile niveau Bac+2 </h3>
                            <div class="subheading mb-3">Simplon.VE Le Cheylard - Ardèche</div>
                        </div>
                        <div class="flex-shrink-0"><span class="text-primary">2020 - 2021</span></div>
                    </div>
                    <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                        <div class="flex-grow-1">
                            <h3 class="mb-0">Master 1 et 2 Négociation Internationale et Interculturelle</h3>
                            <div class="subheading mb-3">Université Aix-Marseille à Aix-en-Provence</div>
                            
                            
                        </div>
                        <div class="flex-shrink-0"><span class="text-primary">2017 - 2019</span></div>
                    </div>
                    <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                        <div class="flex-grow-1">
                            <h3 class="mb-0">Licence 3 Langues Étrangères Appliquées Anglais/Chinois
                                échange universitaire</h3>
                            <div class="subheading mb-3">Kaohsiung - Taïwan - Chine</div>
                            
                            
                        </div>
                        <div class="flex-shrink-0"><span class="text-primary">2016 - 2017</span></div>
                    </div>
                    <div class="d-flex flex-column flex-md-row justify-content-between">
                        <div class="flex-grow-1">
                            <h3 class="mb-0">Licence 1 et 2 Langues Étrangères Appliquées Anglais/Chinois</h3>
                            <div class="subheading mb-3">Université Montpellier - Lyon</div>
                           
                        </div>
                        <div class="flex-shrink-0"><span class="text-primary">2012 - 2016</span></div>
                    </div>
                </div>
            </section>
            <hr class="m-0" />
            <!-- Compétences-->
            <section class="resume-section" id="skills">
                <div class="resume-section-content">
                    <h2 class="mb-5">Compétences</h2>
                    <div class="subheading mb-3">Langages et Frameworks</div>
                    <ul class="list-inline dev-icons">
                    <li class="list-inline-item"><i class="fab fa-html5"></i></li>
                        <li class="list-inline-item"><i class="fab fa-css3-alt"></i></li>
                        <li class="list-inline-item"><i class="fab fa-js-square"></i></li>
                        <li class="list-inline-item"><i class="fab fa-angular"></i></li>
                        <li class="list-inline-item"><i class="fab fa-react"></i></li>
                        <li class="list-inline-item"><i class="fab fa-node-js"></i></li>
                        
                        <li class="list-inline-item"><i class="fab fa-symfony"></i></li>
                        <li class="list-inline-item"><i class="fab fa-wordpress"></i></li>
                        <li class="list-inline-item"><i class="fab fa-php"></i></li>
                        <li class="list-inline-item"><i class="fab fa-git-square"></i></i></li>
                    </ul>
                    <div class="subheading mb-3">Développer la partie front-end d’une application mobile en intégrant les recommandations de sécurité 
                    </div>
                    <ul class="fa-ul mb-0">
                        <li>
                            <span class="fa-li"><i class="fas fa-check"></i></span>
                            Maquetter une application 
                        </li>
                        <li>
                            <span class="fa-li"><i class="fas fa-check"></i></span>
                            Réaliser une interface utilisateur web statique et adaptable  
                        </li>
                        <li>
                            <span class="fa-li"><i class="fas fa-check"></i></span>
                            Développer une interface utilisateur web dynamique  
                        </li>
                        <li>
                            <span class="fa-li"><i class="fas fa-check"></i></span>
                            Réaliser une interface utilisateur avec une solution de gestion de contenu ou e-commerce  
                        </li><br>
                        
                       
                    </ul>
                    <div class="subheading mb-3">Développer la partie back-end d’une application web ou web mobile en intégrant les recommandations de sécurité 
                    </div>
                <ul class="fa-ul mb-0">
                    <li>
                        <span class="fa-li"><i class="fas fa-check"></i></span>
                        Créer une base de données 
                    </li>
                    <li>
                        <span class="fa-li"><i class="fas fa-check"></i></span>
                        Développer les composants d’accès aux données  
                    </li>
                    <li>
                        <span class="fa-li"><i class="fas fa-check"></i></span>
                        Développer la partie back-end d’une application web ou web mobile
                    </li>
                    <li>
                        <span class="fa-li"><i class="fas fa-check"></i></span>
                        Elaborer et mettre en oeuvre des composants dans une application de gestion de contenu ou e-commerce
                    </li>
                </div>
            </section>
            <hr class="m-0" />
            <!-- Interests-->
            
            <hr class="m-0" />
            <!-- Formulaire Contact-->
            <section class="resume-section" id="contact">
                <div class="resume-section-content">
                    <form action="index.php" method="POST">
                        <h2 class="mb-5">Contact</h2>

                        <label id="nom" class="subheading mb-1"> Nom: </label></br>
                        <input class="champ" type="text" name="nom" minlength="2" required></br></br>

                        <label id="mail" class="subheading mb-1"> Mail: </label></br>
                        <input class="champ" type="email" name="email" minlength="7" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required></br></br>

                        <label for="msg" class="subheading mb-1">Message :</label></br>
                        <textarea id="msg" name="message" maxlength="400" required></textarea></br></br>
                        <input id="bouton" class="btn btn-primary" background-color: #3893bd type="submit" value="Envoyer">
                    </form>
                </div>
            </section>

            <?php
            else :


                // 1 - Les données obligatoires sont-elles présentes ?
                if (isset($_POST["email"]) && isset($_POST["message"]) && isset($_POST["nom"])) {

                    // 2 - Les données obligatoires sont-elles remplies ?
                    if (!empty($_POST["email"]) && !empty($_POST["message"] && !empty($_POST["nom"]))) {

                        // 3 - Les données obligatoires ont-elles une valeur conforme à la forme attendue ?
                        if (filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
                            $_POST["email"] = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);

                            if (filter_var($_POST["message"], FILTER_SANITIZE_FULL_SPECIAL_CHARS)) {
                                $_POST["message"] = filter_var($_POST["message"], FILTER_SANITIZE_FULL_SPECIAL_CHARS);

                                if (filter_var($_POST["nom"], FILTER_SANITIZE_FULL_SPECIAL_CHARS)) {


                                    $to      = 'tiba.benzer@gmail.com' ;
                                    $subject = "Vous avez un nouveau contact";
                                    $message = $_POST["nom"].' vous a écrit<br>'. $_POST["mail"];
                                    $message .= PHP_EOL . '<br><br>Contenu du message : <br>' . $_POST["message"];
                                        $headers = 'From: Ratiba.BENZEROUALI@labo-ve.fr '. PHP_EOL .
                                        'Reply-To: ' .$_POST["mail"]. PHP_EOL .
                                        'Content-Type: text/html; charset=UTF-8'. PHP_EOL .
                                        'X-Mailer: PHP/' . phpversion() ;
                                        mail($to, $subject, $message, $headers);
                                                ?>
                                        <section class="resume-section" id="contact">
                                            <div class="resume-section-content">
                                                <div class="container">
                                                    <div class="row">
                                                    <div class="col-10 col-md-5 text-center">
                                                        <h3>Votre message a bien été envoyé</h3></br>
                                                        <h3>Merci</h3></br>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>

                                        <meta http-equiv='Refresh' content='3;URL=index.php'>

                                    <?php
                            
                        
                                
                                } else {
                                    ?>
                                    <h1>'Merci de reformuler votre nom'</h1>
                                    <meta http-equiv='Refresh' content='3;URL=index.php'>

                                <?php
                                }
                            } else {
                                ?>
                                <h1>'Merci de reformuler votre message'</h1>
                                <meta http-equiv='Refresh' content='3;URL=index.php'>

                            <?php
                            }
                        } else {
                            ?>
                            <h1>"Votre adresse mail n'est pas valide"</h1>
                            <meta http-equiv='Refresh' content='3;URL=index.php'>

                        <?php
                        }
                    } else {
                        ?>
                        <h1>"Merci de remplir correctement tous les champs"</h1>
                        <meta http-equiv='Refresh' content='3;URL=index.php'>

                    <?php
                    }
                } else {
                    ?>
                    <h1>"Vos informations ne sont pas valides"</h1>
                    <meta http-equiv='Refresh' content='3;URL=index.php'>

            <?php
                }
            endif;
            ?>


        </div>
        <!-- Bootstrap core JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Third party plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
    </body>
</html>
